insert into ESPECIALISTA(nombre, especialidad) values
('1','1'),
('2','2'),
('3','3');

insert into PACIENTE(identificacion,nombre,direccion,telefono) values 
('1','1','1','1'),
('2','2','2','2'),
('3','3','3','3');

insert into HISTORIA(titulo, descripcion, actores, director, inventario) values
('1','1','1','1',1),
('2','2','2','2',2),
('3','3','3','3',3);

insert into CITA(fecha, especialidad, medico, sede) values
('1','1','1','1'),
('2','2','2','2'),
('3','3','3','3');
