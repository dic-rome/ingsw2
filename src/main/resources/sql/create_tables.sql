-- -----------------------------------------------------
-- Table PACIENTE
-- -----------------------------------------------------
DROP TABLE IF EXISTS ESPECIALISTA;

CREATE TABLE IF NOT EXISTS ESPECIALISTA (
	id INT NOT NULL AUTO_INCREMENT,
	especialidad VARCHAR(45) NOT NULL,
	nombre VARCHAR(45) NOT NULL,
	PRIMARY KEY (id)
);



-- -----------------------------------------------------
-- Table PACIENTE
-- -----------------------------------------------------
DROP TABLE IF EXISTS PACIENTE ;

CREATE TABLE IF NOT EXISTS PACIENTE (
	id INT NOT NULL AUTO_INCREMENT,
	identificacion VARCHAR(45) NOT NULL,
	nombre VARCHAR(45) NOT NULL,
	direccion VARCHAR(255) NULL,
	telefono VARCHAR(45) NULL,
	estado_civil VARCHAR(45) NULL,
	genero VARCHAR(45) NULL,
	eps VARCHAR(45) NULL,
	fecha_nacimiento DATE NULL,
	telefono_emergencia VARCHAR(45) NULL,
	contacto_emergencia VARCHAR(45) NULL,
	relacion_paciente VARCHAR(45) NULL,
	rh VARCHAR(45) NULL,
	PRIMARY KEY (id)
);


-- -----------------------------------------------------
-- Table HISTORIA
-- -----------------------------------------------------
DROP TABLE IF EXISTS HISTORIA ;

CREATE TABLE IF NOT EXISTS HISTORIA (
	id INT NOT NULL AUTO_INCREMENT,
	titulo VARCHAR(45) NOT NULL,
	descripcion VARCHAR(255) NULL,
	actores VARCHAR(255) NULL,
	director VARCHAR(45) NULL,
	inventario VARCHAR(45) NULL,
	PRIMARY KEY (id)
);


-- -----------------------------------------------------
-- Table CITA
-- -----------------------------------------------------
DROP TABLE IF EXISTS CITA ;

CREATE TABLE IF NOT EXISTS CITA (
	id INT NOT NULL AUTO_INCREMENT,
	fecha VARCHAR(45) NULL,
	especialidad VARCHAR(45) NULL,
	medico VARCHAR(45) NULL,
	sede VARCHAR(45) NULL,
	PRIMARY KEY (id)
);
