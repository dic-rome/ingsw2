package com.webapp.clinichistory.paciente;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.webapp.clinichistory.util.MyBatisSqlSessionFactory;

public class PacienteData implements PacienteService {
    
    public PacienteData() {
    }

    public List<PacienteModel> findAll() {
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            PacienteMapper pacienteMapper = sqlSession.getMapper(PacienteMapper.class);
            return pacienteMapper.findAll();
        }finally {
            sqlSession.close();
        }
        // TODO Auto-generated method stub
    }


    public List<PacienteModel> search(PacienteModel o) {
        // TODO Auto-generated method stub
        return null;
    }


    public int add(PacienteModel a) {
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            PacienteMapper pacienteMapper = sqlSession.getMapper(PacienteMapper.class);
            pacienteMapper.add(a);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
        return 0;
    }


    public PacienteModel update(PacienteModel u) {
        // TODO Auto-generated method stub
        return null;
    }


    public PacienteModel remove(PacienteModel r) {
        // TODO Auto-generated method stub
        return null;
    }

    public PacienteModel find(PacienteModel f) {
        // TODO Auto-generated method stub
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            PacienteMapper mapper = sqlSession.getMapper(PacienteMapper.class);
            return mapper.find(f.getId());
        }finally {
            sqlSession.close();
        }
    }

}
