package com.webapp.clinichistory.paciente;

import java.util.Date;

public class PacienteModel {
    
    private long id = -1L;
    private String identificacion="";
    private String nombre="";
    private String direccion="";
    private String telefono=""; 
    private String estadoCivil  = "";
    private String genero = "";
    private String eps = "";
    private String fechaNacimiento ;
    private String telefonoEmergencia = "";
    private String contactoEmergencia = "";
    private String relacionPaciente = "";
    private String rh = "";
    
    public PacienteModel() {
        // TODO Auto-generated constructor stub
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEps() {
		return eps;
	}

	public void setEps(String esp) {
		this.eps = esp;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getTelefonoEmergencia() {
		return telefonoEmergencia;
	}

	public void setTelefonoEmergencia(String telefonoEmergencia) {
		this.telefonoEmergencia = telefonoEmergencia;
	}

	public String getContactoEmergencia() {
		return contactoEmergencia;
	}

	public void setContactoEmergencia(String contactoEmergencia) {
		this.contactoEmergencia = contactoEmergencia;
	}

	public String getRelacionPaciente() {
		return relacionPaciente;
	}

	public void setRelacionPaciente(String relacionPaciente) {
		this.relacionPaciente = relacionPaciente;
	}

	public String getRh() {
		return rh;
	}

	public void setRh(String rh) {
		this.rh = rh;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof PacienteModel)) {
            return false;
        }
        PacienteModel other = (PacienteModel) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    
}
