package com.webapp.clinichistory.paciente;

import java.util.List;

public interface PacienteMapper {
    
    List<PacienteModel> findAll();
    
    void add(PacienteModel paciente);
    
    PacienteModel find(long id);

}
