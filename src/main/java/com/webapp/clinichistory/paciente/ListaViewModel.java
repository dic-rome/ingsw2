package com.webapp.clinichistory.paciente;

import java.io.Serializable;
import java.util.List;

import org.zkoss.bind.annotation.*;

public class ListaViewModel implements Serializable {

    private static final long serialVersionUID = -8079659199182797143L;

    PacienteModel selectedPacienteModel;
    PacienteService service = new PacienteData();
    List<PacienteModel> pacienteListModel;

    public ListaViewModel() {
    }
    

    public PacienteModel getSelectedPacienteModel() {
        return selectedPacienteModel;
    }


    public void setSelectedPacienteModel(PacienteModel selectedPacienteModel) {
        this.selectedPacienteModel = selectedPacienteModel;
    }


    public List<PacienteModel> getPacienteListModel() {
        return pacienteListModel;
    }


    public void setPacienteListModel(List<PacienteModel> pacienteListModel) {
        this.pacienteListModel = pacienteListModel;
    }


    @Init
    public void init() {
        //List<PacienteModel> list = service.getPacienteList();
        pacienteListModel = service.findAll(); // new ListModelList<PacienteModel>(list);
        System.out.println(pacienteListModel.size());
    }

}
