package com.webapp.clinichistory.paciente;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

public class PacienteNuevoController extends PacienteControllerBase {
    
    private static final long serialVersionUID = -3043688825549935517L;
    
    @Wire("#frmPaciente #txtIdentificacion")
    public Textbox txtIdentificacion;
    @Wire("#frmPaciente #txtNombre")
    public Textbox txtNombre;
    @Wire("#frmPaciente #txtDireccion")
    public Textbox txtDireccion;
    @Wire("#frmPaciente #txtTelefono")
    public Textbox txtTelefono;
    @Wire("#frmPaciente #txtEstadoCivil")
    public Textbox txtEstadoCivil;
    @Wire("#frmPaciente #txtGenero")
    public Textbox txtGenero;
    @Wire("#frmPaciente #txtEPS")
    public Textbox txtEPS;
    @Wire("#frmPaciente #dbxFechaNacimiento")
    public Datebox dbxFechaNacimiento;
    @Wire("#frmPaciente #txtTelefonoEmergencia")
    public Textbox txtTelefonoEmergencia;
    @Wire("#frmPaciente #txtContactoEmergencia")
    public Textbox txtContactoEmergencia;
    @Wire("#frmPaciente #txtRelacionPaciente")
    public Textbox txtRelacionPaciente;
    @Wire("#frmPaciente #txtRH")
    public Textbox txtRH;
    
    
    public PacienteNuevoController() {
        // TODO Auto-generated constructor stub
        super();
    }
    
    @Listen("onClick = button#btnAceptar")
    public void onClickAceptar(Event evt) {
        System.out.println("presiono en aceptar:"+this.txtIdentificacion.getValue());
        String identificacion = (txtIdentificacion.getValue().isEmpty()) ? "123456" : txtIdentificacion.getValue();
        String nombre = (txtNombre.getValue().isEmpty()) ? "nombre" : txtNombre.getValue();
        String direccion = (txtDireccion.getValue().isEmpty()) ? "direccion" : txtDireccion.getValue();
        String telefono = (txtTelefono.getValue().isEmpty()) ? "telefono" : txtTelefono.getValue();
        String estadoCivil = txtEstadoCivil.getValue();
        String genero = txtGenero.getValue();
        String esp = txtEPS.getValue();
        String fechaNacimiento = (new SimpleDateFormat("yyyy-MM-dd")).format(dbxFechaNacimiento.getValue());
        String telefonoEmergencia = txtTelefonoEmergencia.getValue();
        String contactoEmergencia = txtContactoEmergencia.getValue();
        String relacionPaciente = txtRelacionPaciente.getValue();
        String rh = txtRH.getValue();
        
        PacienteModel m = new PacienteModel();
        m.setIdentificacion(identificacion);
        m.setNombre(nombre);
        m.setDireccion(direccion);
        m.setTelefono(telefono);
        m.setEstadoCivil(estadoCivil);
        m.setGenero(genero);
        m.setEps(esp);
        m.setFechaNacimiento(fechaNacimiento);
        m.setTelefonoEmergencia(telefonoEmergencia);
        m.setContactoEmergencia(contactoEmergencia);
        m.setRelacionPaciente(relacionPaciente);
        m.setRh(rh);
        
        if (super.getData().add(m) == 0) {
            Messagebox.show("Paciente agregado");
        }
        
    }


}
