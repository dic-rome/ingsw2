package com.webapp.clinichistory.paciente;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;

public abstract class PacienteControllerBase extends SelectorComposer<Component> {

    private static final long serialVersionUID = -177383043851772291L;
    
    private PacienteData data = null;
    
    public PacienteControllerBase() {
        super();
        this.data = new PacienteData();
        
    }

    /**
     * @return the data
     */
    public PacienteData getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(PacienteData data) {
        this.data = data;
    }
    
    

}
