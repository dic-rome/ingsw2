package com.webapp.clinichistory.historia;

import java.util.List;

public interface HistoriaMapper {
    List<HistoriaModel> findAll();
    void add(HistoriaModel historia);
    HistoriaModel find(long id);
}
