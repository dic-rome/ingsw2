package com.webapp.clinichistory.historia;

public class HistoriaModel {
    long id = -1L;
    String titulo = "";
    String descripcion = "";
    String actores = "";
    String director = "";
    String  inventario = "";

    public HistoriaModel() {
        // TODO Auto-generated constructor stub
    }

    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getActores() {
        return actores;
    }

    public void setActores(String actores) {
        this.actores = actores;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }


    public String getInventario() {
		return inventario;
	}

	public void setInventario(String inventario) {
		this.inventario = inventario;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof HistoriaModel)) {
            return false;
        }
        HistoriaModel other = (HistoriaModel) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    
}
