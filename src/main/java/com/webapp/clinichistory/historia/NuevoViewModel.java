package com.webapp.clinichistory.historia;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.zkoss.bind.annotation.*;
import org.zkoss.zul.Messagebox;

import com.webapp.clinichistory.paciente.PacienteData;
import com.webapp.clinichistory.paciente.PacienteModel;
import com.webapp.clinichistory.paciente.PacienteService;

public class NuevoViewModel implements Serializable {

	private static final long serialVersionUID = -3842737550609329204L;

	HistoriaModel nuevoModelo;
	HistoriaService historiaService = new HistoriaData();
	PacienteService pacienteService = new PacienteData();
	PacienteModel selectedPacienteModel;
	List<String> listaNombre;

	public List<String> getListaNombre() {
		return listaNombre;
	}

	public void setListaNombre(List<String> listaNombre) {
		this.listaNombre = listaNombre;
	}

	public NuevoViewModel() {
		// TODO Auto-generated constructor stub
	}

	public HistoriaModel getNuevoModelo() {
		return nuevoModelo;
	}

	public void setNuevoModelo(HistoriaModel nuevoModelo) {
		this.nuevoModelo = nuevoModelo;
	}

	@Init
	public void init() {
		nuevoModelo = new HistoriaModel();
		listaNombre = pacienteService.findAll().stream().map(PacienteModel::getNombre).collect(Collectors.toList()); // new ListModelList<PacienteModel>(list);
		System.out.println("Historias Nueva: init");
	}

	@Command
	// @NotifyChange("nuevoModelo")
	public void addNuevo() {
		historiaService.add(nuevoModelo);
		if (Messagebox.show("Hidtoria agregada") > 0) {
			// Executions.sendRedirect("../historia/lista.zul");
		}
	}

	public PacienteModel getSelectedPacienteModel() {
		return selectedPacienteModel;
	}

	public void setSelectedPacienteModel(PacienteModel selectedPacienteModel) {
		this.selectedPacienteModel = selectedPacienteModel;
	}


}
