package com.webapp.clinichistory.historia;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.webapp.clinichistory.util.MyBatisSqlSessionFactory;

public class HistoriaData implements HistoriaService {
    
    private static long pID = 0;
    private static List<HistoriaModel> historias = new ArrayList<HistoriaModel>();

    public HistoriaData() {
    }

    public List<HistoriaModel> findAll() {
        // TODO Auto-generated method stub
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            HistoriaMapper mapper = sqlSession.getMapper(HistoriaMapper.class);
            return mapper.findAll();
        }finally {
            sqlSession.close();
        }
        // return historias;
    }

    public List<HistoriaModel> search(HistoriaModel o) {
        // TODO Auto-generated method stub
        return null;
    }

    public int add(HistoriaModel a) {
        // TODO Auto-generated method stub
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            HistoriaMapper mapper = sqlSession.getMapper(HistoriaMapper.class);
            mapper.add(a);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
        //historias.add(a);
        return 0;
    }

    public HistoriaModel update(HistoriaModel u) {
        // TODO Auto-generated method stub
        return null;
    }

    public HistoriaModel remove(HistoriaModel r) {
        // TODO Auto-generated method stub
        return null;
    }

    public HistoriaModel find(HistoriaModel f) {
        // TODO Auto-generated method stub
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            HistoriaMapper mapper = sqlSession.getMapper(HistoriaMapper.class);
            return mapper.find(f.getId());
        }finally {
            sqlSession.close();
        }
        //return historias.get(historias.indexOf(f));
    }
    
    public static long getID() {
        return ++pID;
    }

}
