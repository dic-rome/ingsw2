package com.webapp.clinichistory.historia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModelList;


public class ListaViewModel implements Serializable {

    private static final long serialVersionUID = -2740276161360819238L;

    HistoriaModel selectedHistoriaModel;
    HistoriaService service = new HistoriaData();
    List<HistoriaModel> historiaListModel;
    List<HistoriaModel> listBoxModel;
	List<String> listaNombre;
	ListModelList<HistoriaModel> modelList;
	String selectedNombre;
    
    public ListaViewModel() {
        // TODO Auto-generated constructor stub
    }
    
    public HistoriaModel getSelectedHistoriaModel() {
        return selectedHistoriaModel;
    }

    public void setSelectedHistoriaModel(HistoriaModel selectedHistoriaModel) {
        this.selectedHistoriaModel = selectedHistoriaModel;
    }


    public List<HistoriaModel> getHistoriaListModel() {
        return historiaListModel;
    }

    public void setHistoriaListModel(List<HistoriaModel> historiaListModel) {
        this.historiaListModel = historiaListModel;
    }

    @Init
    public void init() {
        historiaListModel = service.findAll();
        listBoxModel = new ArrayList<HistoriaModel>(historiaListModel);
        listaNombre = historiaListModel.stream().map(HistoriaModel::getTitulo).distinct().collect(Collectors.toList());
        System.out.println("historias:init");
    }

	public List<String> getListaNombre() {
		return listaNombre;
	}

	public void setListaNombre(List<String> listaNombre) {
		this.listaNombre = listaNombre;
	}
	
	
	public String getSelectedNombre() {
		return selectedNombre;
	}

	public void setSelectedNombre(String selectedNombre) {
		this.selectedNombre = selectedNombre;
	}

	@Command("onSelectNombre")
	@NotifyChange("listBoxModel")
	public void onSelectNombre() {
		final String value = selectedNombre;
		listBoxModel = historiaListModel.stream().filter(iten -> Objects.equals(iten.getTitulo(), value)).collect(Collectors.toList());
	}

	public List<HistoriaModel> getListBoxModel() {
		return listBoxModel;
	}

	public void setListBoxModel(List<HistoriaModel> listBoxModel) {
		this.listBoxModel = listBoxModel;
	}

}
