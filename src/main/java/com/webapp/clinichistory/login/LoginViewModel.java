package com.webapp.clinichistory.login;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

public class LoginViewModel {

	private UserModel user;

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	// con el @command se le dice que metodo debe llamar o mejor dicho se marca el metodo y en el zul se le dice cual llamar en el onclick
	@Command
	@NotifyChange("user")
	public void login() {
		System.out.println("hola login");
		user.setName("cambio");
	}

	@Init
	public void init() {
		user = new UserModel();
		user.setName("username");;
	}

}
