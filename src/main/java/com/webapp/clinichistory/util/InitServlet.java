package com.webapp.clinichistory.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.webapp.clinichistory.data.LoadData;

public class InitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public InitServlet() {
        LoadData.testConnection();
        LoadData.insertData();
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	PrintWriter out = resp.getWriter();
    	resp.setContentType("application/json");
    	resp.setCharacterEncoding("UTF-8");
    	out.print("{\"result\":\"database OK\"}");
    	out.flush();
    	super.doGet(req, resp);
    }

}
