package com.webapp.clinichistory.especialista;

import java.util.List;

public interface EspecialistaMapper {
	List<EspecialistaModel> findAll();
    void add(EspecialistaModel historia);
    EspecialistaModel find(long id);
}
