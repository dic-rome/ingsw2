package com.webapp.clinichistory.especialista;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.webapp.clinichistory.util.MyBatisSqlSessionFactory;

public class EspecialistaData implements EspecialistaService {

    private static long pID = 0;

	@Override
	public List<EspecialistaModel> findAll() {
		SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            EspecialistaMapper mapper = sqlSession.getMapper(EspecialistaMapper.class);
            return mapper.findAll();
        }finally {
            sqlSession.close();
        }
	}

	@Override
	public List<EspecialistaModel> search(EspecialistaModel o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int add(EspecialistaModel a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public EspecialistaModel update(EspecialistaModel u) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EspecialistaModel remove(EspecialistaModel r) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EspecialistaModel find(EspecialistaModel f) {
		SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
        	EspecialistaMapper mapper = sqlSession.getMapper(EspecialistaMapper.class);
            return mapper.find(f.getId());
        }finally {
            sqlSession.close();
        }
	}
	
	public static long getID() {
        return ++pID;
    }

}
