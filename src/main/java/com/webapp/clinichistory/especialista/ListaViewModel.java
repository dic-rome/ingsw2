package com.webapp.clinichistory.especialista;

import java.io.Serializable;
import java.util.List;

import org.zkoss.bind.annotation.Init;

public class ListaViewModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7203764381346614081L;
	EspecialistaModel selectedEspecialistaModel;
	EspecialistaService service = new EspecialistaData();
	List<EspecialistaModel> especialistaListModel;
	
	@Init
    public void init() {
        especialistaListModel = service.findAll();
        System.out.println("especialista:init");
    }

	public EspecialistaModel getSelectedEspecialistaModel() {
		return selectedEspecialistaModel;
	}

	public void setSelectedEspecialistaModel(EspecialistaModel selectedEspecialistaModel) {
		this.selectedEspecialistaModel = selectedEspecialistaModel;
	}

	public EspecialistaService getService() {
		return service;
	}

	public void setService(EspecialistaService service) {
		this.service = service;
	}

	public List<EspecialistaModel> getEspecialistaListModel() {
		return especialistaListModel;
	}

	public void setEspecialistaListModel(List<EspecialistaModel> especialistaListModel) {
		this.especialistaListModel = especialistaListModel;
	}
	
	
}
