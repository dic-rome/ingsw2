package com.webapp.clinichistory.cita;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.zkoss.bind.annotation.*;
import org.zkoss.zul.Messagebox;

public class NuevoViewModel implements Serializable {

	private static final long serialVersionUID = -3314202996544882282L;
	private String especialidad;
	private CitaModel cita;
	private Date fecha;
	private CitaService service = new CitaData();

	public Date getFecha() {
		if (cita != null && cita.getFecha() != null) {
			try {
				fecha = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).parse(cita.getFecha());
			} catch (ParseException e) {
			}
		}
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
		if (cita != null) {
			cita.setFecha((new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).format(fecha));
		}
	}

	public CitaModel getCita() {
		return cita;
	}

	public void setCita(CitaModel cita) {
		this.cita = cita;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	@Init
	void init() {
		cita = new CitaModel();
		System.out.println("Citas Nuevo: init");
	}

	@Command
    @NotifyChange("nuevoModelo")
    public void addNuevo() {
		service.add(cita);
		Messagebox.show("Cita Registrada");
    }
	
	@Command
	public void onSelectEspecilidad() {
		cita.setEspecialidad(especialidad);
	}

}
