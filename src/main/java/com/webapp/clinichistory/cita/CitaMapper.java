package com.webapp.clinichistory.cita;

import java.util.List;

public interface CitaMapper {
    List<CitaModel> findAll();
    void add(CitaModel cita);
    
}
