package com.webapp.clinichistory.cita;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.webapp.clinichistory.util.MyBatisSqlSessionFactory;

public class CitaData implements CitaService {
    
    private static List<CitaModel> citas = new ArrayList<CitaModel>();

    public CitaData() {
        // TODO Auto-generated constructor stub
    }

    public List<CitaModel> findAll() {
        // TODO Auto-generated method stub
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            CitaMapper mapper = sqlSession.getMapper(CitaMapper.class);
            return mapper.findAll();
        }finally {
            sqlSession.close();
        }
        //return citas;
    }

    public List<CitaModel> search(CitaModel o) {
        // TODO Auto-generated method stub
        return null;
    }

    public int add(CitaModel a) {
        // TODO Auto-generated method stub
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlsession();
        try {
            CitaMapper mapper = sqlSession.getMapper(CitaMapper.class);
            mapper.add(a);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
        //citas.add(a);
        return 0;
    }

    public CitaModel update(CitaModel u) {
        // TODO Auto-generated method stub
        return null;
    }

    public CitaModel remove(CitaModel r) {
        // TODO Auto-generated method stub
        return null;
    }

    public CitaModel find(CitaModel f) {
        // TODO Auto-generated method stub
        return null;
    }

}
