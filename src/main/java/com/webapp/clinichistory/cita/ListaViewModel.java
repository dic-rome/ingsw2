package com.webapp.clinichistory.cita;

import java.io.Serializable;
import java.util.List;

import org.zkoss.bind.annotation.*;

public class ListaViewModel implements Serializable {

    private static final long serialVersionUID = 1739341847464790752L;
    
    CitaModel selectedCita;
    List<CitaModel> citas;
    CitaService service = new CitaData();

    public CitaModel getSelectedCita() {
        return selectedCita;
    }

    public void setSelectedCita(CitaModel selectesCita) {
        this.selectedCita = selectesCita;
    }

    public List<CitaModel> getCitas() {
        return citas;
    }

    public void setCitas(List<CitaModel> citas) {
        this.citas = citas;
    }

    public ListaViewModel() {
        // TODO Auto-generated constructor stub
    }
    
    @Init
    public void init(){
        citas = service.findAll();
        System.out.println("Citas:init");
    }

}
